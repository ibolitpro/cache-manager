<?php

namespace Ibolit\Cacher;

/**
 * Class CacheDriver - base class for implementations of cache storages
 * @package ibolit\cacher
 */
abstract class CacheDriver
{
    /**
     * @param string $key
     *
     * @return mixed
     */
    abstract public function get(string $key);

    /**
     * @param string $key - base key
     * @param CacheElemVO $element - Value Object of caching element
     * @return bool
     */
    abstract public function set(string $key, CacheElemVO $element): ?bool;

    /**
     * @param string $key
     *
     * @return bool
     */
    abstract public function del(string $key): ?bool;

    /**
     * @param string $key
     *
     * @return mixed
     */
    abstract public function getSetMembers(string $key): ?array;

    /**
     * @param string $pattern
     * @return mixed
     */
    abstract public function invalidateByTime(string $pattern);

    /**
     * @param string $key
     * @param $cacheName
     * @return bool|null
     */
    abstract public function addToSet(string $key, $cacheName): bool;

    /**
     * @return bool
     */
    abstract public function isConnected(): bool;
}
