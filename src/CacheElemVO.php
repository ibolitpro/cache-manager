<?php

namespace Ibolit\Cacher;

/**
 * Class CacheElemVO - Value object class for caching element properties
 * @package Ibolit\Cacher
 */
class CacheElemVO
{
    /**
     * Caching data
     * @var null|string
     */
    private $cacheData;

    /**
     * Non-personalized Cache name
     * @var string
     */
    private $name;

    /**
     * UserId for cache key personalizing
     * @var int|null
     */
    private $uid;

    /**
     * Expired date
     * @var int
     */
    private $expired;

    /**
     * @return string|null
     */
    public function getCacheData(): ?string
    {
        return $this->cacheData;
    }

    /**
     * @param string $cacheData
     */
    public function setCacheData(?string $cacheData): void
    {
        $this->cacheData = $cacheData;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }


    /**
     * @return int|null
     */
    public function getUid(): ?int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid(int $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return int
     */
    public function getExpired(): int
    {
        return $this->expired;
    }

    /**
     * @param int $expired
     */
    public function setExpired(int $expired): void
    {
        $this->expired = $expired;
    }

    /**
     * Data validation
     * @return bool
     */
    public function isSetValid(): bool
    {
        if (empty($this->cacheData) || empty($this->name)) {
            return false;
        }

        return true;
    }

    /**
     * Data validation
     * @return bool
     */
    public function isGetValid(): bool
    {
        if (empty($this->name)) {
            return false;
        }

        return true;
    }

}