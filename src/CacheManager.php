<?php

namespace Ibolit\Cacher;

/**
 * Class CacheManager - main top level class for caching mechanics
 * @package Ibolit\Cacher
 */
class CacheManager
{

    /**
     * Key prefixes
     */
    const KEY_ELEM_PREFIX = 'elem';
    const KEY_DICT_INV_PREFIX = 'dict:dep';

    const KEY_DIVIDER = ':';

    /**
     * @var CacheDriver
     */
    private $cacheInstance;

    /**
     * CacheManager constructor.
     *
     * @param CacheDriver $instance - instance of cache storage driver
     */
    public function __construct(CacheDriver $instance)
    {
        $this->cacheInstance = $instance;
    }


    /**
     * @param CacheElemVO $cacheElement
     *
     * @return CacheElemVO|null
     */
    public function get(CacheElemVO $cacheElement): ?CacheElemVO
    {
        if (!$cacheElement->isGetValid()) {
            return null;
        }

        $key = $this->buildElemKey($cacheElement);
        $cacheElement->setCacheData($this->cacheInstance->get($key));

        return $cacheElement;
    }

    /**
     * @param CacheElemVO $cacheElement
     *
     * @return bool|null
     */
    public function set(CacheElemVO $cacheElement): ?bool
    {
        if (!$cacheElement->isSetValid()) {
            return null;
        }

        $key = $this->buildElemKey($cacheElement);
        $status = $this->cacheInstance->set($key, $cacheElement);


        if ($status === true) {
            $this->invalidateDependencies($cacheElement);
        }

        return $status;

    }

    /**
     * @param CacheElemVO $cacheElement
     *
     * @return bool
     */
    public function del(CacheElemVO $cacheElement): bool
    {
        if (!$cacheElement->isGetValid()) {
            return false;
        }
        $key = $this->buildElemKey($cacheElement);

        return $this->cacheInstance->del($key);
    }

    /**
     * Asynchronous cache invalidator (based on generators)
     *
     * @param $pattern
     */
    public function invalidateByTime(string $pattern): void
    {
        foreach ($this->cacheInstance->invalidateByTime($pattern) as $keys) {
            foreach ($keys as $key) {
                $vo = $this->parseElemKey($key);
                $this->del($vo);
                $this->invalidateDependencies($vo);
            }
        }
    }

    /** Adding dependent cache names to dictionary
     *
     * @param CacheElemVO $cacheElem
     * @param CacheElemVO $cacheDep
     *
     * @return bool
     */
    public function setDependency(CacheElemVO $cacheElem, CacheElemVO $cacheDep): bool
    {

        if (!($cacheElem->isGetValid() && $cacheDep->isGetValid())) {
            return false;
        }

        $key = $this->buildDictDependenciesKey($cacheElem);

        return $this->cacheInstance->addToSet($key, $cacheDep->getName());
    }

    /**
     * Dependent keys invalidations after changing (setting / resetting) simple cache
     *
     * @param CacheElemVO $cacheElement
     */
    private function invalidateDependencies(CacheElemVO $cacheElement): void
    {
        $invKey = $this->buildDictDependenciesKey($cacheElement);
        $dependencyMembers = $this->cacheInstance->getSetMembers($invKey);
        if (is_array($dependencyMembers)) {
            foreach ($dependencyMembers as $member) {
                $cacheInvElem = new CacheElemVO();
                $cacheInvElem->setUid($cacheElement->getUid());
                $cacheInvElem->setName($member);
                $this->del($cacheInvElem);
            }
        }
    }

    /**
     * Key builder for caching elements
     *
     * @param CacheElemVO $parameters
     *
     * @return string
     */
    private function buildElemKey(CacheElemVO $parameters): string
    {
        $uidOptional = (empty($parameters->getUid()) ? '' : self::KEY_DIVIDER . $parameters->getUid());

        return self::KEY_ELEM_PREFIX . self::KEY_DIVIDER . $parameters->getName() . $uidOptional;
    }

    /**
     * Key builder for inverted index mapping
     *
     * @param CacheElemVO $parameters
     *
     * @return string
     */
    private function buildDictDependenciesKey(CacheElemVO $parameters): string
    {
        return self::KEY_DICT_INV_PREFIX . self::KEY_DIVIDER . $parameters->getName();
    }

    /** Cache key parser
     *
     * @param $key
     *
     * @return CacheElemVO|null
     */
    private function parseElemKey($key): ?CacheElemVO
    {
        $pattern = '/' . CacheManager::KEY_ELEM_PREFIX . CacheManager::KEY_DIVIDER .
            '([^:]+)(' . CacheManager::KEY_DIVIDER . '(\d+)){0,1}/';

        if (!preg_match_all($pattern, $key, $matches)) {
            return null;
        } else {
            $vo = new CacheElemVO();
            $vo->setName($matches[1][0]);
            if ((int)$matches[3][0] > 0) {
                $vo->setUid($matches[3][0]);
            }

            return $vo;
        }
    }
}
