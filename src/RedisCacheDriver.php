<?php

namespace Ibolit\Cacher;

class RedisCacheDriver extends CacheDriver
{
    /**
     * Instance of php Redis extension class
     *
     * @var \Redis
     */
    private $redisInstance;

    /**
     * State of connection
     *
     * @var bool
     */
    private $redisConnectionStatus = false;

    /**
     * @var string
     */
    private $namespace = '';

    /**
     * RedisCacheDriver constructor.
     *
     * @param string $namespace
     * @param string $host
     * @param string|null $password
     * @param int $port
     * @param int $dbNumber
     */
    public function __construct(
        string $namespace,
        string $host,
        ?string $password = null,
        int $port = 6379,
        int $dbNumber = 0
    )
    {
        try {
            $this->redisInstance = new \Redis();
            $this->redisConnectionStatus = $this->redisInstance->connect($host, $port);
            $this->redisInstance->auth($password);
            $this->redisInstance->select($dbNumber);
            $this->redisInstance->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE);
            $this->redisInstance->setOption(\Redis::OPT_SCAN, \Redis::SCAN_RETRY);
            $this->redisInstance->setOption(\Redis::OPT_PREFIX, $namespace);
            $this->namespace = $namespace;
        }
        catch (\Exception $e){
            $this->redisConnectionStatus = false;
        }
    }

    /**
     * Returns connection status
     *
     * @return bool
     *
     * @throws \RedisException
     */
    public function isConnected(): bool
    {
        if($this->redisConnectionStatus === true && !empty($this->redisInstance->ping("ping"))) {
            return true;
        }
        
        return false;
    }

    /**
     * Getting data
     *
     * @param string $key
     *
     * @return string|null
     */
    public function get(string $key):?string
    {
        if (!$this->isConnected()) {
            return null;
        }

        $expired = $this->redisInstance->hGet($key, 'expires_at');

        if (!empty((int)$expired) && time() > $expired) {
            return null;
        }

        $result = $this->redisInstance->hGet($key, 'data');

        if (false === $result) {
            return null;
        }

        $this->redisInstance->hSet($key, 'last_hit', time());
        $this->redisInstance->hIncrBy($key, 'hit_count', 1);

        return $result;
    }

    /**
     * Setting data
     *
     * @param string $key
     * @param string $data
     *
     * @return bool
     */
    public function set(string $key, CacheElemVO $data): ?bool
    {
        if (!$this->isConnected()) {
            return null;
        }

        $this->redisInstance->hSet($key, 'data', $data->getCacheData());
        $this->redisInstance->hSet($key, 'created_at', time());
        $this->redisInstance->hSet($key, 'hit_count', 1);
        $this->redisInstance->hSet($key, 'last_hit', '0');
        if (is_int($data->getExpired())) {
            $this->redisInstance->hSet($key, 'expires_at', $data->getExpired());
        }

        return true;
    }

    /**
     * @param string $key
     * @param $cacheName
     *
     * @return bool
     */
    public function addToSet(string $key, $cacheName): bool
    {
        if (!$this->isConnected()) {
            return false;
        }

        if ($this->redisInstance->sAdd($key, $cacheName) === false){
            return false;
        }

        return true;
    }

    /**
     * Deleting data
     *
     * @param string $key
     *
     * @return bool
     */
    public function del(string $key): bool
    {
        if (!$this->isConnected()) {
            return false;
        }

        return $this->redisInstance->del($key);
    }

    /**
     * Getting data from SET data structure using for storing inverted keys of complex cache elements
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getSetMembers(string $key):?array
    {
        $members = $this->redisInstance->sMembers($key);

        if (is_array($members) && count($members) > 0) {
            return $members;
        }

        return null;
    }

    /**
     * Invalidation by time
     *
     * @param string $pattern
     *
     * @return array
     */
    public function invalidateByTime(string $pattern)
    {
        $it = null;

        do {
            $keys = $this->redisInstance->scan($it, $this->namespace . $pattern, 10000);
            $keysExpired = [];

            if (is_array($keys)) {

                foreach ($keys as $key){
                    $key = str_replace($this->namespace, '', $key);

                    $expired = $this->redisInstance->hGet($key , 'expires_at');

                    if((int)$expired && time() > $expired){
                        $keysExpired[] = $key;
                    }
                }

                yield $keysExpired;
            }
        } while ($keys !== false);
    }
}